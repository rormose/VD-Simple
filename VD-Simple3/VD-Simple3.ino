


// Yet unucr simpl pitx-kuntrol p4tx fr cu Volku Drumz "split mod".
//
// Al drum-voysiz get individjwul pitx-kuntrol uv ytx leyur, bay sepreytiq leyurz antw ceyr on MIDI tx4nlz.
// Cyz leyurz ar kuntrold bay nots an tx4nlz 0 tu 11 (aka 1 tu 12).
//
// Leyrr-spisifik Kuntrol-Txeyndj kum4ndz wil by madifayd tw uplay tw cu tx4nl cey wr sent an, nat cu leyrr cey wr sent an.
// Also, eny Paly Prexr 4nd Tx4nl Prexr kum4ndz wil by intrpritid 4z "ENV-UM4WNT" CCz for cer karispandiq voys-leyrr.
//
// Leyurz wic u laq rilys-taym wil by intrruptid bay not-afs. Cer leyur-valywm wil by set tw 0 bay en udixinl CC an ytx NOT-AF.
// WARNIQ: Meyk xr nots in yr extrnl sykwensr h4v u drreyxn, so cu leyurz cer kuntroliq dont imydyitly mywt cemselvz.
//
// NOT: Meyk xr tw txeynj cu v4lywz in cy "4FTRTUTX" urey, u fyw laynz bilo cis p4rugr4f, tw riflekt yr dizayrrd 4FTRTUTX biheyvyr.
//
//
//
// Yet another simple pitch-control patch for the Volca Drum's "split mode".
//
// All drum-voices get individual pitch-control of each layer, by separating layers onto their own MIDI channels.
// These layers are controlled by notes on channels 0 to 11 (aka 1 to 12).
//
// Layer-specific Control Change commands will be modified to apply to the channel they were sent on, not the layer they were sent on.
// Also, any Poly Pressure and Channel Pressure commands will be interpreted as "ENV-AMOUNT" CCs for their corresponding voice-layer.
//
// Layers with a long release-time will be interrupted by NOTE-OFFs. Their layer-volume will be set to 0 by an additional CC on each NOTE-OFF.
// WARNING: Make sure notes in your external sequencer have a duration, so the layers they're controlling don't immediately mute themselves.
//
// NOTE: Make sure to change the values in the "AFTERTOUCH" array, a few lines below this paragraph, to reflect your desired AFTERTOUCH behavior.



// Include the RK-002 cable's library
#include <RK002.h>



// RK002 declarations
RK002_DECLARE_INFO("VD-Simple2", "sevenplagues@gmail.com", "0.1", "15964836-2f39-4558-8c8b-55ec2220a9e1")



// Toggles whether a given channel's corresponding layer will respond to AFTERTOUCH commands or not (128, MSB, is either on or off),
// ... and what value it will return to when a note is released (0 to 127, in the other 7 bits).
const byte AFTERTOUCH[13] = { 
    0,   0, 192, 192,
  192, 192,   0,   0,
    0,   0,   0,   0,
  255 // This final dummy-bit is included for Arduino paranoia reasons
};

// Global variables
byte IS_INITIALIZED = 0; // Flag for whether initialization-CCs have already been sent or not.
byte SUSTAINS[13] = { // Each pitched-voice's current sustained note (128 when off)
  128, 128, 128, 128,
  128, 128, 128, 128,
  128, 128, 128, 128,
  128 // This final dummy-bit is included for Arduino paranoia reasons
};



bool RK002_onNoteOn(byte chan, byte key, byte velo) {

  if (!IS_INITIALIZED) { // If initialization-CCs have not yet been sent...
  
    // Mute both layers of every voice.
    RK002_sendControlChange(0, 19, 0);
    RK002_sendControlChange(1, 19, 0);
    RK002_sendControlChange(2, 19, 0);
    RK002_sendControlChange(3, 19, 0);
    RK002_sendControlChange(4, 19, 0);
    RK002_sendControlChange(5, 19, 0);
    
    IS_INITIALIZED = 1; // Set the "is this initialized yet" flag to "yes".
    
  }

  byte layer; // Will be: current layer's value.
  byte drum; // Will be: current drum.
  
  if (chan <= 11) { // If this is a pitched-voice note...
  
    layer = chan % 2; // Get the current layer that's being played (derive it from the given channel).
    drum = chan >> 1; // Since each "drum" contains two voices, get the drum that the current channel corresponds to.
    
    SUSTAINS[chan] = key; // Store the voice's sustained-note.
  
    RK002_sendControlChange(drum, 26 + layer, key); // Send a pitch-change to the current layer of the current drum.
    
  } else { // Else, if this is on any other channel, send the note through.
  
    return true;
    
  }
  
  RK002_sendControlChange(drum, 17 + layer, velo); // Send a velocity-change to the current layer of the current drum.
  
  RK002_sendNoteOn(drum, key, velo); // Send a note-on to the current drum.
  
  return false; // A modified note-on has already been sent, so don't allow the original note through.

}

bool RK002_onNoteOff(byte chan, byte key, byte velo) {

  byte layer; // Will be: current layer's value.
  byte drum; // Will be: current drum.
  
  if (chan <= 11) { // If this is a pitched-voice note...

    layer = chan % 2; // Get the current layer that's being played (derive it from the given channel).
    drum = chan >> 1; // Since each drum-voice occupies two channels, get the voice that the current channel corresponds to.
    
    if (AFTERTOUCH[chan] & 128) { // If AFTERTOUCH is enabled for this layer...
      RK002_sendControlChange(drum, 29 + layer, AFTERTOUCH[chan] & 127); // Reset the layer's ENV-AMOUNT to its default state.
    }
    
    if (SUSTAINS[chan] != key) { return false; } // If this NOTE-OFF isn't for the voice's sustained note, do nothing.
    
    SUSTAINS[chan] = 128; // Set the voice's current-sustain flag to "empty".
    
  } else { // Else, if this is on any other channel, send the note through.
  
    return true;
  
  }
  
  // Otherwise, this is a NOTE-OFF for the voice's currently-sustained note. So...

  RK002_sendControlChange(drum, 17 + layer, 0); // Mute the velocity of the current layer of the current drum-voice.
  
  RK002_sendNoteOff(drum, key, velo); // Send a note-off to the current drum-voice (this is mostly a formality, in case of weird routing setups).
  
  return false; // A modified note-off has already been sent, so don't allow the original note through.

}

bool RK002_onControlChange(byte chan, byte b1, byte b2) {

  if (chan <= 11) { // If this is a pitched-voice note...

    byte layer = chan % 2; // Get the current layer that's being played (derive it from the given channel).
    byte drum = chan >> 1; // Since each drum-voice occupies two channels, get the voice that the current channel corresponds to.
    
    if (
      (b1 == 14)
      || (b1 == 17)
      || (b1 == 20)
      || (b1 == 23)
      || (b1 == 26)
      || (b1 == 29)
      || (b1 == 46)
    ) { // If this CC command is for layer-1...
      b1 += layer; // Bring it in line with the given layer-number (+0 for layer 1, +1 for layer 2)
    } else if (
      (b1 == 15)
      || (b1 == 21)
      || (b1 == 24)
      || (b1 == 27)
      || (b1 == 30)
      || (b1 == 47)
    ) { // Otherwise, if this CC command is for layer 2...
      b1 -= !layer; // Bring it in line with the given layer-number (-1 for layer 1, -0 for layer 2)
    }
    
    RK002_sendControlChange(drum, b1, b2); // Send a CC that has been adjusted for the correct voice and parameter.
    
    return false; // A modified control-change has already been sent, so don't allow the original command through.
    
  }
  
  return true; // ...If this was on any other channel, send the command through.

}

bool RK002_onPolyPressure(byte chan, __attribute__((unused)) byte b1, byte b2) {
  return parseAftertouch(chan, b2);
}
bool RK002_onChannelPressure(byte chan, byte b1) {
  return parseAftertouch(chan, b1);
}
bool parseAftertouch(byte chan, byte b1) { // AFTERTOUCH is treated as a special call to its corresponding layer's "AMOUNT" CC

  if (chan <= 11) { // If this is a pitched-voice note...

    if (AFTERTOUCH[chan] & 128) { // If AFTERTOUCH is enabled for this layer...
    
      // Send an AMOUNT CC that has been adjusted for the correct drum-voice, layer, and parameter.
      RK002_sendControlChange(chan >> 1, 29 + (chan % 2), b1);
      
    }
    
    return false; // A modified control-change has either been sent or been nullified, so don't allow the original command through.
    
  }
  
  return true; // ...If this was on any other channel, send the command through.

}



// Needs setup and loop funcs or else it won't work
void setup() {}
void loop() {}



